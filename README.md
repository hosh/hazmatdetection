For better test:
Increase network-resolution by set in your .cfg-file (height=608 and width=608) or (height=832 and width=832) or (any value multiple of 32) - this increases the precision and makes it possible to detect small objects
but to get even greater accuracy you should train with higher resolution 608x608 or 832x832, note: if error Out of memory occurs then in .cfg-file you should increase subdivisions=16, 32 or 64



to train:  ./darknet detector train data/Hazmat.data data/yolov3.cfg darknet53.conv.74 -show_imgs


to test image :  ./darknet detector test data/Hazmat.data data/yolov3.cfg  backup/yolov3_500.weights haz -thresh 0.05 -ext_output <data/train.txt> result.json


to test video: ./darknet detector demo data/Hazmat.data data/yolov3.cfg  backup/yolov3_1800.weights  -thresh 0.12 /dev/video2 -ext_output <data/train.txt> result.json

